# 全网招投标信息网管理系统

#### 项目介绍
    全网招投标信息网管理系统为招投标企业服务，实现自动获取中国国内国家招投标信息，包括预告、招标公告、更正公告、评标结果、中标结果等信息的采集获取。
     可定制关键字，为某些行业定制某些门类，实现定时发送、定制跟踪关键等等。

     每日定时生成PDF文件，以邮件发送的方式发送到管理者邮箱，邮箱绑定短信后，即可实现时时获取招投标信息，将全网的招投标信息一览无遗，不会出现人工查看遗漏情况的发生。

#### 软件架构
软件架构说明


#### 安装教程

1. pom
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<groupId>com.itssky</groupId>
	<artifactId>crawler</artifactId>
	<version>2.0-beta</version>
	<packaging>jar</packaging>

	<properties>
		<javamail.version>1.4.7</javamail.version>

		<lucene.version>3.0.1</lucene.version>
		<maven.build.timestamp.format>yyyyMMddHHmmss</maven.build.timestamp.format>
	</properties>
	<dependencies>
		<dependency>
			<groupId>net.sf.json-lib</groupId>
			<artifactId>json-lib</artifactId>
			<version>2.3</version>
			<classifier>jdk15</classifier>
		</dependency>
		<dependency>
			<groupId>com.googlecode.juniversalchardet</groupId>
			<artifactId>juniversalchardet</artifactId>
			<version>1.0.3</version>
		</dependency>


		<dependency>
			<groupId>org.jsoup</groupId>
			<artifactId>jsoup</artifactId>
			<version>1.10.3</version>
		</dependency>

		<dependency>
			<groupId>commons-dbutils</groupId>
			<artifactId>commons-dbutils</artifactId>
			<version>1.6</version>
		</dependency>

		<dependency>
			<groupId>commons-dbcp</groupId>
			<artifactId>commons-dbcp</artifactId>
			<version>1.4</version>
		</dependency>

		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>5.1.6</version>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.6.1</version>
		</dependency>

		<dependency>
			<groupId>javax.mail</groupId>
			<artifactId>mail</artifactId>
			<version>${javamail.version}</version>
		</dependency>

		<!-- 定时任务 -->
		<dependency>

			<groupId>org.quartz-scheduler</groupId>
			<artifactId>quartz</artifactId>
			<version>2.2.2</version>
		</dependency>
		<!-- ITEXT -->

		<dependency>
			<groupId>com.itextpdf</groupId>
			<artifactId>itextpdf</artifactId>
			<version>5.5.1</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.itextpdf.tool/xmlworker -->
		<dependency>
			<groupId>com.itextpdf.tool</groupId>
			<artifactId>xmlworker</artifactId>
			<version>5.4.1</version>
		</dependency>

		<dependency>
			<groupId>com.itextpdf</groupId>
			<artifactId>itext-asian</artifactId>
			<version>5.2.0</version>
		</dependency>

		<!-- selenium -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
			<version>2.44.0</version>
		</dependency>
		<!-- phantomjsdriver(selenium webdriver 第三方支持) -->
		<dependency>
			<groupId>com.github.detro</groupId>
			<artifactId>phantomjsdriver</artifactId>
			<version>1.2.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpcore</artifactId>
			<version>4.4.3</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.5.6</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/net.sourceforge.htmlunit/htmlunit -->
		<dependency>
			<groupId>net.sourceforge.htmlunit</groupId>
			<artifactId>htmlunit</artifactId>
			<version>2.29</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.apache.lucene/lucene-core -->
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-core</artifactId>
			<version>${lucene.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.lucene/lucene-analyzers-common -->
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-analyzers</artifactId>
			<version>${lucene.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.lucene/lucene-queries -->
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-queries</artifactId>
			<version>${lucene.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-memory</artifactId>
			<version>${lucene.version}</version>
		</dependency>



		<!-- https://mvnrepository.com/artifact/org.apache.lucene/lucene-highlighter -->
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-highlighter</artifactId>
			<version>${lucene.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.lucene/lucene-queryparser -->
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-queryparser</artifactId>
			<version>${lucene.version}</version>
		</dependency>


		<!-- https://mvnrepository.com/artifact/com.janeluo/ikanalyzer -->
		<dependency>
			<groupId>com.janeluo</groupId>
			<artifactId>ikanalyzer</artifactId>
			<version>2012_u6</version>
		</dependency>


		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.9</version>
			<scope>test</scope>
		</dependency>

		<!-- 引用外部JAR，需要在这定义 -->

		<dependency>
			<groupId>swftools</groupId>
			<artifactId>core</artifactId>
			<version>1.0.0</version>
			<scope>system</scope>
			<systemPath>${project.basedir}/lib/SWFTools-Core.jar</systemPath>
		</dependency>
		<dependency>
			<groupId>transform</groupId>
			<artifactId>core</artifactId>
			<version>1.0.0</version>
			<scope>system</scope>
			<systemPath>${project.basedir}/lib/transform.jar</systemPath>
		</dependency>


	</dependencies>

	<build>
		<!-- ${maven.build.timestamp} ${project.artifactId}-${project.version}-${buildNumber} -->
		<finalName>${project.artifactId}-${maven.build.timestamp}</finalName>
		<defaultGoal>compile</defaultGoal>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<includes>
					<include>**/*.*</include>
				</includes>

				<filtering>false</filtering>
			</resource>
			<resource>
				<directory>src/main/java</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
				<excludes>
					<exclude>**/*.java</exclude>
				</excludes>
				<filtering>false</filtering>
			</resource>
			<resource>
				<directory>lib</directory>
				<includes>
					<include>**/*.*</include>
				</includes>
				<excludes>
					<exclude>Tesseract-OCR/*.*</exclude>
				</excludes>
				<filtering>false</filtering>
			</resource>

		</resources>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.12.3</version>
				<configuration>
					<groups>unit</groups>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.7.0</version>

				<configuration>
					<source>1.8</source>
					<target>1.8</target>
					<project.build.sourceEncoding>utf-8</project.build.sourceEncoding>

				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>3.1.0</version>

				<!-- configuration> <archive> <manifestFile>${project.build.outputDirectory}/META-INF/MANIFEST.MF</manifestFile> 
					<manifest> <mainClass>com.crawl.timer.CrawlerTaskTimer</mainClass> </manifest> 
					</archive> </configuration -->
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-shade-plugin</artifactId>
				<version>3.1.0</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>shade</goal>
						</goals>
						<!-- configuration> <transformers> <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer"> 
							<mainClass>com.crawl.timer.CrawlerTaskTimer</mainClass> </transformer> </transformers> 
							</configuration -->
					</execution>
				</executions>
			</plugin>
		

		</plugins>
	</build>
</project>
2. 外部jar
SWFTools-Core.jar
transform.jar
3. xxxx

#### 使用说明

1. 运行命令行：
java -Dfile.encoding=utf-8 -jar  crawler-20180917081641.jar com.crawl.timer.CrawlerTaskTimer



>java -Dfile.encoding=utf-8 -classpath crawler-20180918021723.jar com.crawl.timer.CrawlerWorker 1 0

依赖绝对路径。

e:\Tesseract-OCR

2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)